<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class)->times(50)->make();
        User::insert($users->toArray());

        $user = User::find(2);
        $user->name = 'HollyTeng';
        $user->email = '281615005@qq.com';
        $user->password = 'css0929';
        $user->is_admin = true;
        $user->activated = true;
        $user->save();
    }
}
